#!/bin/sh

set -eu

export HOME=/tmp
TASKD_PKI_DIR='/usr/share/taskd/pki'
TASKD_DATA_DIR='/var/lib/taskd'
TASKD_USER='Debian-taskd'

# Generate certificates and keys for taskd
cd "$TASKD_PKI_DIR" || exit 1
sudo -u "$TASKD_USER" ./generate >/dev/null 2>&1

# Launch the taskd server
sudo -u "$TASKD_USER" taskd server --data "$TASKD_DATA_DIR" --daemon

# Create an organization
sudo -u "$TASKD_USER" taskd add org Public --data "$TASKD_DATA_DIR"

# Create a user and get the user key
user_key=$(
  sudo -u "$TASKD_USER" \
  taskd add user Public user --data "$TASKD_DATA_DIR" \
  | awk '/New user key:/ {print $4}'
)

# Generate certificate and key for the client
sudo -u "$TASKD_USER" ./generate.client user >/dev/null 2>&1

# Initialize taskwarrior
mkdir -p ~/.task
echo 'data.location=/tmp/.task' > ~/.taskrc

# Copy certificate and key files to taskwarrior folder
for file in user.cert.pem user.key.pem ca.cert.pem; do
  sudo -u "$TASKD_USER" cat "$TASKD_PKI_DIR/$file" | tee ~/.task/"$file" >/dev/null
done

# Configure taskwarrior to use certificates
yes | task config taskd.certificate -- ~/.task/user.cert.pem
yes | task config taskd.key -- ~/.task/user.key.pem
yes | task config taskd.ca -- ~/.task/ca.cert.pem

# Configure taskserver in taskwarrior
yes | task config taskd.server -- localhost:53589
yes | task config taskd.credentials -- Public/user/"$user_key"

# Create a task in taskwarrior
task add Testing taskd

# Test taskwarrior and taskd connection
if ! output=$(yes | task sync init) || [ ! "$output" == *"Sync successful"* ];
then
  exit 1
fi
