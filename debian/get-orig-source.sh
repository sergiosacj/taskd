#!/bin/bash
# called by uscan with '--upstream-version' <version>

set -eux

if [ -z "$1" ]; then
  echo "Error: Argument '--upstream-version' is missing" 1>&2
  exit 1
fi

if [ "$1" != "--upstream-version" ]; then
  echo "Error: First argument must be '--upstream-version'" 1>&2
  exit 1
fi

if [ -z "$2" ]; then
  echo "Error: Version argument is missing" 1>&2
  exit 1
fi

UPSTREAM_VERSION="$2"
BRANCH_NAME=$( echo "${UPSTREAM_VERSION}" | awk -F'~' '{print $1}' )
DEB_SOURCE="$( dpkg-parsechangelog -SSource )"
TARBALL="$( readlink -f ../"${DEB_SOURCE}"_"${UPSTREAM_VERSION}".orig.tar.xz )"
WORK_DIR="$( mktemp -d )"
trap 'rm -rf "${WORK_DIR}"' EXIT

git clone \
  --branch "${BRANCH_NAME}" \
  --depth 1 --recursive \
  https://github.com/GothenburgBitFactory/taskserver.git \
  "${WORK_DIR}"

tar --exclude '.git*' -caf "${TARBALL}" "${WORK_DIR}"
